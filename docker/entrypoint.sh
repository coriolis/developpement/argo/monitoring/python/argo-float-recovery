#!/bin/bash
APP_NAME=argo-float-recovery
[[ "_${ENVIRONMENT_FILE}" != "_" ]] && export ENVIRONMENT_FILE="${ENVIRONMENT_FILE}"
echo "*** Starting ${APP_NAME} - environment: ${ENVIRONMENT_FILE} ***"
python -m main