"""App module."""
import os
import dash
from dash.dependencies import Input, Output
from dash import html
import dash_bootstrap_components as dbc
import flask
from environment.settings import APP_DEBUG, DEV_TOOLS_PROPS_CHECK
from application.utils.shared.constants import APPLICATION_NAME
from application.layout import layout
from application.pages.dashboard import dashboard


def create_dash_app(routes_pathname_prefix: str = None) -> dash.Dash:
    """Sample Dash application from Plotly: https://github.com/plotly/dash-hello-world/blob/main/app.py."""
    server = flask.Flask(__name__)
    server.secret_key = os.environ.get('secret_key', 'secret')

    app = dash.Dash(
        __name__,
        server=server,
        routes_pathname_prefix=routes_pathname_prefix,
        suppress_callback_exceptions=True,
        external_stylesheets=[
            dbc.themes.BOOTSTRAP,
            'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css'
        ],
        meta_tags=[
            {"name": "viewport", "content": "width=device-width, initial-scale=1"}
        ],
    )

    # Define title of application
    app.title = APPLICATION_NAME
    # Define layout for application
    app.layout = layout
    # Config dev tool for application
    app.enable_dev_tools(
        dev_tools_serve_dev_bundles=APP_DEBUG,
        dev_tools_props_check=DEV_TOOLS_PROPS_CHECK,
        debug=APP_DEBUG,
        dev_tools_hot_reload=APP_DEBUG)
    # app.scripts.config.serve_locally = False
    app.css.config.serve_locally = True
    app.scripts.config.serve_locally = True


    @app.callback(Output("page-content", "children"), [Input("url", "pathname")])
    def render_page_content(pathname):
        """Define dashboard route."""
        if pathname == routes_pathname_prefix:
            return dashboard()

        if pathname.startswith("/float"):
            return dashboard(pathname.rsplit('/', 1)[-1])

        return html.Div(
            dbc.Container(
                [
                    html.H1("404: Not found", className="text-danger"),
                    html.Hr(className="my-2"),
                    html.P(f"The pathname {pathname} was not recognised..."),
                ],
                fluid=True,
                className="py-3",
            ),
            className="p-5 bg-light rounded-3",
            style={
                "marginTop": "6vh",
                "color": "#000",
            },
        )

    return app
