ARG IMAGE_REGISTRY="gitlab-registry.ifremer.fr/ifremer-commons/docker/images/"
ARG DOCKER_IMAGE="python:3.11-slim"
# ======================================================================================================================
# Stage python-base is used to build base project image
# ======================================================================================================================
FROM gitlab-registry.ifremer.fr/ifremer-commons/docker/images/python:3.11-slim AS python-base
ENV PYTHONUNBUFFERED=1 \
    PYTHONDONTWRITEBYTECODE=1 \
    PIP_NO_CACHE_DIR=off \
    PIP_DISABLE_PIP_VERSION_CHECK=on \
    PIP_DEFAULT_TIMEOUT=100 \
    POETRY_HOME="/opt/poetry" \
    POETRY_VIRTUALENVS_IN_PROJECT=true \
    POETRY_NO_INTERACTION=1 \
    PYSETUP_PATH="/opt/pysetup" \
    VENV_PATH="/opt/pysetup/.venv"

ENV PATH="$POETRY_HOME/bin:$VENV_PATH/bin:$PATH"

# ======================================================================================================================
# Stage builder-base is used to build dependencies
# ======================================================================================================================
FROM python-base AS builder-base

# Install dependencies (curl, git)
RUN apt-get -y update \
    && apt-get -y upgrade \
    && apt-get install --no-install-recommends --no-install-suggests -y curl=7.88.1-10+deb12u7 git=1:2.39.5-0+deb12u1  \
    && apt-get -y purge --auto-remove -o APT::AutoRemove::RecommendsImportant=false \
    && rm -rf /var/lib/apt/lists/*

# Install Poetry - respects $POETRY_VERSION & $POETRY_HOME
ENV POETRY_VERSION=1.4.0
SHELL ["/bin/bash", "-o", "pipefail", "-c"]
RUN curl -sSL https://install.python-poetry.org | python3  && \
    chmod a+x /opt/poetry/bin/poetry

# We copy our Python requirements here to cache them
# and install only runtime deps using poetry
WORKDIR $PYSETUP_PATH
COPY ./poetry.lock ./pyproject.toml ./
RUN \
    # git init && \
    poetry add poetry-dynamic-versioning==0.14.0 && \
    poetry install --only main --no-root

# ======================================================================================================================
# 'develop' stage installs all dev deps and can be used to develop code.
# For example using docker-compose to mount local volume under /app
# ======================================================================================================================
FROM builder-base AS development

# venv already has runtime deps installed we get a quicker install
WORKDIR $PYSETUP_PATH
RUN poetry install --with dev --no-root


# ======================================================================================================================
# 'production' stage uses the clean 'python-base' stage and copyies
# in only our runtime deps that were installed in the 'builder-base'
# ======================================================================================================================
FROM python-base AS runtime

# Build arg
ARG ENVFILE=".env.production"
ARG USERNAME="udocker"
ARG USERID="9999"
ARG GROUPNAME="gcontainer"
ARG GROUPID="9999"

# Create execution user/group
RUN \
echo "Creation group ${GROUPNAME}:${GROUPID}" && \
    groupadd -g ${GROUPID} ${GROUPNAME} && \
    echo "Creatin user ${USERNAME}:${USERID}" && \
    useradd -l -g ${GROUPNAME} -d /app -u ${USERID} ${USERNAME}

USER ${USERNAME}:${GROUPNAME}
WORKDIR /app

COPY --from=builder-base $VENV_PATH $VENV_PATH
COPY --chown=${USERNAME}:${GROUPNAME} . /app

# Defult environment variables
ENV HDF5_USE_FILE_LOCKING=FALSE
ENV ENVIRONMENT_FILE=$ENVFILE

USER ${USERNAME}

ENTRYPOINT ["/app/docker/entrypoint.sh"]
