"""Application settings."""
import os
from os.path import join, dirname
from dotenv import load_dotenv
import yaml


dotenv_path = join(dirname(__file__), os.getenv("ENVIRONMENT_FILE") or ".env.local")
load_dotenv(dotenv_path=dotenv_path, override=True)


APP_HOST = os.environ.get("HOST")
APP_PORT = int(os.environ.get("PORT"))
APP_DEBUG = bool(os.environ.get("DEBUG"))
DEV_TOOLS_PROPS_CHECK = bool(os.environ.get("DEV_TOOLS_PROPS_CHECK"))
FLOAT_DATA_DIR = os.environ.get("FLOAT_DATA_DIR")
EXT_CONFIG_FILE = os.environ.get("EXT_CONFIG_FILE")
SERVER_URL = os.environ.get("SERVER_URL")

EXTERNAL_CONFIG = {}


def load_external_configurations():
    """Load external YAML configurations."""
    with open(EXT_CONFIG_FILE, "r") as stream:
        try:
            global EXTERNAL_CONFIG
            EXTERNAL_CONFIG = yaml.safe_load(stream)
        except yaml.YAMLError as exc:
            print(exc)


def get_configurations():
    """Get YAML configurations."""
    return EXTERNAL_CONFIG
