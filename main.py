"""Main module!"""
import uvicorn

from starlette.middleware.wsgi import WSGIMiddleware
from app import create_dash_app
from environment.settings import APP_HOST, APP_PORT
from application.services.shared_service import app

# FIXME: Why we need to declare contollers in a specific order ???
# Declare components controllers
import application.components.datatable.datatable_controller  # pylint: disable=W0611

# A bit odd, but the only way I've been able to get prefixing of the Dash app
# to work is by allowing the Dash/Flask app to prefix itself, then mounting
# it to root
dash_app = create_dash_app(routes_pathname_prefix="/")
app.mount("/", WSGIMiddleware(dash_app.server))

# Declare pages controllers
import application.pages.dashboard_controller  # noqa: E402 pylint: disable=C0413,W0611
import application.components.map.map_controller  # noqa: F401,E402 pylint: disable=C0413,W0611
import application.components.shared.navbar_controller  # noqa: F401,E402 pylint: disable=C0413,W0611
import application.components.shared.login_controller  # noqa: F401,E402 pylint: disable=C0413,W0611
import application.components.shared.sidebar_controller  # noqa: F401,E402 pylint: disable=C0413,W0611

if __name__ == "__main__":
    # Server FastApi for production
    uvicorn.run(app, host=APP_HOST, port=APP_PORT, timeout_keep_alive=15)
