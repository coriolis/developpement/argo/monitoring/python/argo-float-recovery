## FLOAT-RECOVERY 1.0.0 - 17th January 2022
#### Evolutions :
* **Ticket 4** : Ifremer's vessels locations are now available.
* **Ticket 12** : Non Ifremer vessels locations can be requested after some administration configuration.
* **Ticket 15** : It's now possible to add forms on the map.
* **Ticket 16** : Coordinates are now diplay at bottom left corner of the map on click.

## FLOAT-RECOVERY 0.0.6 - 29th June 2021
#### Evolutions :
* **Ticket 6** : It's now possible to download sources data files from last float location on the map.

## FLOAT-RECOVERY 0.0.5 - 24th June 2021
#### Evolutions :
* **Ticket 3** : Application is now responsive.
* **Ticket 8** : Direct link to float monitoring page in new designed map popup.
* **Ticket 9** : Table data are now downloadable.