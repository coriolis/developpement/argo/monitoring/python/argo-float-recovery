# Argo float recovery
Argo float recovery is a lightweight web application designed with Python Dash framework. Its purpose is to trace the location of Argo float to recover. The application reads the location of the floats to be recovered and share them in real time (refresh every minutes).

**Technical documentation** : <https://gitlab.ifremer.fr/coriolis/developpement/argo/monitoring/python/argo-float-recovery#argo-float-recovery>

## Usage
A sidebar allows users to choose how to display and filter data.  

**Display modes** : offers 2 differents ways to consult data : in table form or on a map.  
**Filtering** : A dropdown field allows you to select the float you want to trace in a list. If no float is selected the application will share the latest position of each float.

## Accessibilité
<https://floatrecovery.euro-argo.eu>
