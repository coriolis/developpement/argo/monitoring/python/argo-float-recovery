"""Boat model."""


from application.model.api.geomar_vessel import GeomarVessel
from application.model.api.ifremer_vessel import IfremerVessel
from application.model.api.vtexplorer_vessel import VTExplorerVessel
from application.model.enums import VesselType


class Vessel:  # pylint: disable=R0903
    """Vessel class."""

    def __init__(self, vessel):  # pylint: disable=W0622,R0913
        """Constructor."""
        if isinstance(vessel, IfremerVessel):
            self.id = vessel.id  # pylint: disable=C0103,W0622
            self.name = vessel.name
            self.code = vessel.sismerId
            self.type = VesselType.IFREMER.value
            self.url = vessel.url
            self.avatar = vessel.avatar
        if isinstance(vessel, GeomarVessel):
            self.id = vessel.shortname  # pylint: disable=C0103,W0622
            self.name = vessel.name
            self.code = vessel.callsign
            self.type = VesselType.GEOMAR.value
        if isinstance(vessel, VTExplorerVessel):
            self.id = vessel.MMSI  # pylint: disable=C0103,W0622
            self.name = vessel.NAME
            self.code = vessel.IMO
            self.type = VesselType.VTEXPLORER.value

    def __str__(self):
        """String description."""
        return f"Vessel : {self.id}, {self.name}, {self.code}, {self.url}, {self.avatar}"
