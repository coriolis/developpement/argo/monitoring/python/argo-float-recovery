"""OAuth2 token model."""

from typing import Optional
from pydantic import BaseModel  # pylint: disable=E0611


class Token(BaseModel):  # pylint: disable=R0903
    """OAuth2 token class."""
    access_token: str
    token_type: str


class TokenData(BaseModel):  # pylint: disable=R0903
    """OAuth2 token data class."""
    username: Optional[str] = None
