"""Location model."""


from datetime import datetime
from application.model.api.ifremer_location import IfremerLocation, IfremerLocationData
from application.model.api.vtexplorer_vessel import VTExplorerVessel
from application.utils.shared.constants import DATE_FEATURE_FORMAT


class VesselLocation:  # pylint: disable=R0903
    """Generic location class."""

    def __init__(self, location):
        """Constructor."""
        if isinstance(location, IfremerLocation):
            self.date = datetime.strptime(location.date, DATE_FEATURE_FORMAT).strftime(DATE_FEATURE_FORMAT)
            self.lat = location.lat
            self.lon = location.lon
            if location.data:
                self.data = LocationData(location.data)
            else:
                self.data = None
        if isinstance(location, VTExplorerVessel):
            self.date = datetime.strptime(location.TIMESTAMP, '%Y-%m-%d %H:%M:%S %Z') \
                            .strftime(DATE_FEATURE_FORMAT) + "+0000"
            self.lat = location.LATITUDE
            self.lon = location.LONGITUDE
            self.data = LocationData(location)

    def __str__(self):
        """String description."""
        return f"Location : {self.date}, {self.lat}, {self.lon}, data({self.data})"


class LocationData:  # pylint: disable=R0903,R0902
    """Generic location data class."""

    def __init__(self, data):  # pylint: disable=R0913
        """Constructor."""
        if isinstance(data, IfremerLocationData):
            self.seatemp = data.seatemp
            self.depth = data.depth
            self.truewinddir = data.truewinddir
            self.airtemp = data.airtemp
            self.pressure = data.pressure
            self.truewindspeed = data.truewindspeed
        if isinstance(data, VTExplorerVessel):
            self.course = data.COURSE
            self.speed = data.SPEED
            self.heading = data.HEADING
            self.destination = data.DESTINATION
            self.zone = data.ZONE

    def __str__(self):
        """String description."""
        return f"LocationData : {self}"
