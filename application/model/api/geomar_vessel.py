"""Geomar vessel API model."""


class GeomarVessel:  # pylint: disable=R0903
    """Geomar boat class."""

    def __init__(self, id, callsign, name, shortname):  # pylint: disable=W0622,C0103
        """Constructor."""
        self.id = id  # pylint: disable=C0103
        self.name = name
        self.callsign = callsign
        self.shortname = shortname

    def __str__(self):
        """String description."""
        return f"GeomarVessel : {self.id}, {self.callsign}, {self.name}, {self.shortname}"
