"""Ifremer GeoJson."""
from typing import List
from application.model.api.ifremer_vessel import IfremerVessel


class IfremerFeatureCollection:  # pylint: disable=R0903
    """Ifremer feature collection class."""

    def __init__(self, features, type):  # pylint: disable=W0622
        """Constructor."""
        self.features: List[IfremerFeature] = features
        self.type = type  # pylint: disable=W0622

    def __str__(self):
        """String description."""
        return f"IfremerFeatureCollection : {self.features}, {self.type}"


class IfremerFeature:  # pylint: disable=R0903
    """Geomar feature class."""

    def __init__(self, geometry, type, properties):  # pylint: disable=W0622
        """Constructor."""
        self.geometry = geometry
        self.type = type  # pylint: disable=W0622
        self.properties: IfremerProperties = properties

    def __str__(self):
        """String description."""
        return f"IfremerFeatureCollection : {self.type}, {self.geometry}, {self.properties}"


class IfremerProperties:  # pylint: disable=R0903
    """Ifremer properties class."""

    def __init__(self, date, data, vessel):
        """Constructor."""
        self.date = date
        self.data = data
        self.vessel: IfremerVessel = vessel

    def __str__(self):
        """String description."""
        return f"IfremerFeatureCollection : {self.date}, {self.vessel}, {self.data}"
