"""Geomar GeoJson."""
from typing import List


class GeomarFeatureCollection:  # pylint: disable=R0903
    """Geomar feature collection class."""

    def __init__(self, features, type):  # pylint: disable=W0622
        """Constructor."""
        self.features: List[GeomarFeature] = features
        self.type = type  # pylint: disable=W0622

    def __str__(self):
        """String description."""
        return f"GeomarFeatureCollection : {self.features}, {self.type}"


class GeomarFeature:  # pylint: disable=R0903
    """Geomar feature class."""

    def __init__(self, geometry, type, properties):  # pylint: disable=W0622
        """Constructor."""
        self.geometry = geometry
        self.type = type  # pylint: disable=W0622
        self.properties: GeomarProperties = properties

    def __str__(self):
        """String description."""
        return f"GeomarFeature : {self.type}, {self.geometry}, {self.properties}"


class GeomarProperties:  # pylint: disable=R0903
    """Geomar properties class."""

    def __init__(self, data_origin, platform, leg_label=None, end_date=None, start_date=None):  # pylint: disable=R0913
        """Constructor."""
        self.data_origin = data_origin
        self.platform = platform
        self.leg_label = leg_label
        self.begin_date = start_date
        self.end_date = end_date

    def __str__(self):
        """String description."""
        return f"GeomarProperties : {self.data_origin}, {self.platform}, {self.leg_label}"
