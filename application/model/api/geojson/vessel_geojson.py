"""Vessel GeoJson."""
from datetime import date, datetime
from application.model.api.geojson.geomar_geojson import GeomarFeature, GeomarFeatureCollection, GeomarProperties
from application.model.api.geojson.ifremer_geojson import IfremerFeature, IfremerFeatureCollection, IfremerProperties
from application.model.api.ifremer_vessel import IfremerVessel
from application.model.vessel import Vessel
from application.utils.shared.constants import DATE_FEATURE_FORMAT, DATE_POPUP_FORMAT


class VesselFeatureCollection:  # pylint: disable=R0903
    """Generic vessel feature collection class."""

    def __init__(self, feature_collection):
        """Constructor."""
        if isinstance(feature_collection, IfremerFeatureCollection):
            features = []
            for feature in feature_collection.features:
                features.append(VesselFeature(IfremerFeature(**feature)))
            self.features = features
            self.type = feature_collection.type
        if isinstance(feature_collection, GeomarFeatureCollection):
            features = []
            for feature in feature_collection.features:
                features.append(VesselFeature(GeomarFeature(**feature)))
            self.features = features
            self.type = feature_collection.type

    def __str__(self):
        """String description."""
        return f"VesselFeatureCollection : {self.features}, {self.type}"


class VesselFeature:  # pylint: disable=R0903
    """Generic vessel feature class."""

    def __init__(self, feature):
        """Constructor."""
        if isinstance(feature, IfremerFeature):
            self.geometry = feature.geometry
            self.type = feature.type
            self.properties = VesselProperties(IfremerProperties(**feature.properties))
        if isinstance(feature, GeomarFeature):
            self.geometry = feature.geometry
            self.type = feature.type
            self.properties = VesselProperties(GeomarProperties(**feature.properties))

    def __str__(self):
        """String description."""
        return f"VesselFeature : {self.type}, {self.geometry}, {self.properties}"


class VesselProperties:  # pylint: disable=R0903
    """Generic vessel properties class."""

    def __init__(self, properties):
        """Constructor."""
        if isinstance(properties, IfremerProperties):
            self.date = properties.date
            self.data = properties.data
            self.vessel = Vessel(IfremerVessel(**properties.vessel))
        if isinstance(properties, GeomarProperties):
            parsed_date = properties.data_origin.split("_")[-1]
            if parsed_date is date:
                self.date = datetime.strptime(parsed_date, DATE_POPUP_FORMAT).strftime(DATE_FEATURE_FORMAT) + "+0000"
            else:
                self.date = datetime.utcnow().date().strftime(DATE_FEATURE_FORMAT) + "+0000"
            self.data = dict(
                platform=properties.platform,
                leg_label=properties.leg_label,
                begin_date=properties.begin_date,
                end_date=properties.end_date,
            )
            # self.vessel = Vessel(GeomarVessel(**properties))

    def __str__(self):
        """String description."""
        return f"VesselProperties : {self.date}, {self.vessel}, {self.data}"
