"""Ifemer vessel API model."""


class IfremerVessel:  # pylint: disable=R0903
    """Ifemer boat class."""

    def __init__(self, id, name, sismerId, url, avatar):  # pylint: disable=W0622,R0913,C0103
        """Constructor."""
        self.id = id  # pylint: disable=W0622,C0103
        self.name = name
        self.sismerId = sismerId  # pylint: disable=C0103
        self.url = url
        self.avatar = avatar

    def __str__(self):
        """String description."""
        return f"IfremerVessel : {self.id}, {self.name}, {self.sismerId}, {self.url}, {self.avatar}"
