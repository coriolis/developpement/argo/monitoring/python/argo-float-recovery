"""Ifemer vessel API model."""


class IfremerLocation:  # pylint: disable=R0903
    """Ifemer location class."""

    def __init__(self, date, lat, lon, data):
        """Constructor."""
        self.date = date
        self.lat = lat
        self.lon = lon
        if data:
            self.data = IfremerLocationData(**data)
        else:
            self.data = None

    def __str__(self):
        """String description."""
        return f"IfremerLocation : {self.date}, {self.lat}, {self.lon}, data({self.data})"


class IfremerLocationData:  # pylint: disable=R0903
    """Ifemer location data class."""

    def __init__(self, seatemp=None, depth=None, truewinddir=None, airtemp=None, pressure=None, truewindspeed=None):  # pylint: disable=R0913
        """Constructor."""
        self.seatemp = seatemp
        self.depth = depth
        self.truewinddir = truewinddir
        self.airtemp = airtemp
        self.pressure = pressure
        self.truewindspeed = truewindspeed

    def __str__(self):
        """String description."""
        return f"IfremerLocationData : {self.seatemp}, {self.depth}, {self.truewinddir}, " \
            f"{self.airtemp}, {self.pressure}, {self.truewindspeed}"
