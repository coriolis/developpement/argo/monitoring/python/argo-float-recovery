"""VT explorer vessel API model."""


class VTExplorerVessel:    # pylint: disable=C0103,R0902,R0913,R0914,R0903
    """VT explorer vessel class."""

    def __init__(
        self,
        MMSI,
        TIMESTAMP,
        LATITUDE,
        LONGITUDE,
        COURSE,
        SPEED,
        HEADING,
        NAVSTAT,
        IMO,
        NAME,
        CALLSIGN,
        TYPE,
        A,
        B,
        C,
        D,
        DRAUGHT,
        DESTINATION,
        LOCODE,
        ETA_AIS,
        ETA,
        SRC,
        ZONE,
        ECA,
        DISTANCE_REMAINING,
        ETA_PREDICTED,
    ):
        """Constructor."""
        self.MMSI = MMSI
        self.TIMESTAMP = TIMESTAMP
        self.LATITUDE = LATITUDE
        self.LONGITUDE = LONGITUDE
        self.COURSE = COURSE
        self.SPEED = SPEED
        self.HEADING = HEADING
        self.IMO = IMO
        self.NAME = NAME
        self.DESTINATION = DESTINATION
        self.NAVSTAT = NAVSTAT
        self.CALLSIGN = CALLSIGN
        self.TYPE = TYPE
        self.A = A
        self.B = B
        self.C = C
        self.D = D
        self.DRAUGHT = DRAUGHT
        self.LOCODE = LOCODE
        self.ETA_AIS = ETA_AIS
        self.ETA = ETA
        self.SRC = SRC
        self.ECA = ECA
        self.ZONE = ZONE
        self.DISTANCE_REMAINING = DISTANCE_REMAINING
        self.ETA_PREDICTED = ETA_PREDICTED

    def __str__(self):
        """String description."""
        return f"VTExplorerVessel : {self.MMSI}, {self.NAME}, {self.SPEED}, {self.HEADING}"
