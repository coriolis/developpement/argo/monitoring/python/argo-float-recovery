"""User model."""

from typing import Optional
from pydantic import BaseModel  # pylint: disable=E0611


class User(BaseModel):  # pylint: disable=R0903
    """User class."""
    username: str
    email: Optional[str] = None
    full_name: Optional[str] = None
    disabled: Optional[bool] = None


class UserInDB(User):  # pylint: disable=R0903
    """User in db class."""
    hashed_password: str
