"""Enumerations model."""

from enum import Enum


class VesselType(Enum):
    """Vessel types enumeration."""
    IFREMER = "ifremer"
    GEOMAR = "geomar"
    VTEXPLORER = "vtexplorer"
