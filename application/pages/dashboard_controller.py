"""Dashboard controller, contains callbacks from dashboard view."""
import sys
from main import dash_app


class HaltCallback(Exception):
    """Catch exception callback."""
    pass  # pylint: disable=W0107


@dash_app.server.errorhandler(HaltCallback)
def handle_error(error):
    """Print exception in stderr."""
    print(error, file=sys.stderr)
    return ('', 204)
