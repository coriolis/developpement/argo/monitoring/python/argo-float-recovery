"""Dashboard view."""
from dash import html

from application.components.shared.sidebar import sidebar
from application.components.map.map_view import map_view


def main(float_wmo: str = None):
    """Initialize page content."""
    return html.Div(
        id='tabs-display-content',
        className='col-12 col-lg-10 p-0',
        children=[
            map_view(float_wmo)
        ]
    )


def dashboard(float_wmo: str = None):
    """Initialize dashboard view."""
    return html.Div(
        className='row h-100',
        children=[
            sidebar(float_wmo),
            main(float_wmo),
        ],
        style={
            "marginTop": "6vh",
        }
    )
