"""Float services."""
import json
from os import walk, path
import pandas as pd
from fastapi.responses import JSONResponse
from pandas.core.frame import DataFrame
from geojson import Feature, FeatureCollection, Point, LineString
from environment.settings import FLOAT_DATA_DIR
from application.services.shared_service import app
from application.utils.shared.constants import LAT_COLUMN_NAME, LON_COLUMN_NAME, WMO_COLUMN_NAME, DATE_COLUMN_NAME


@app.get("/get-float-list", response_class=JSONResponse)
async def get_json_float_list():
    """Get float list."""
    floatlist = get_float_list()
    return JSONResponse(content=floatlist, status_code=200)


@app.get("/get-floats-latest/", response_class=JSONResponse)
async def get_floats_latest_locations(output_format: str = "geojson",):  # pylint: disable=W0622
    """Get lastest location of each float."""
    floatlist = get_float_list()
    dframes = []
    for float_wmo in floatlist:
        file_path = path.join(FLOAT_DATA_DIR, float_wmo + ".csv")
        # read csv
        if path.exists(file_path):
            fdf = pd.read_csv(file_path, delimiter=",")
            fdf.sort_values(by=[DATE_COLUMN_NAME], inplace=True, ascending=False, ignore_index=True)
            float_df = fdf.head(1)
            # adding filename to df if geojson to add download data link in the map popup
            float_df_copy = float_df.copy()
            if output_format.lower() == 'geojson':
                float_df_copy['FILENAME'] = [float_wmo]
            dframes.append(float_df_copy)
            # dfs.append(pd.read_csv(file_path, delimiter=",", nrows=1))
    # concat and sort
    dframe = pd.concat(dframes, ignore_index=True)
    dframe.sort_values(by=[DATE_COLUMN_NAME], inplace=True, ascending=False, ignore_index=True)
    # geojson format
    if output_format.lower() == 'geojson':
        response = concat_dataframe_to_geojson(dframe)
    # json format
    else:
        response = dataframe_to_json(dframe)
    return JSONResponse(content=response, status_code=200)


@app.get("/get-float/{float_wmo}", response_class=JSONResponse)
async def get_float_locations(float_wmo: str, output_format: str = "json"):  # pylint: disable=W0622
    """Get wmo float locations."""
    file_path = path.join(FLOAT_DATA_DIR, float_wmo + ".csv")
    dframe = {}
    if path.exists(file_path):
        dframe = pd.read_csv(file_path, delimiter=",")
        # geojson format
        if output_format.lower() == 'geojson':
            # ascending=true to display latest point over others on a map
            dframe.sort_values(by=[DATE_COLUMN_NAME], inplace=True, ascending=True, ignore_index=True)
            response = dataframe_to_geojson(float_wmo, dframe)
        # json format
        else:
            # ascending=false to get latest point at the beginning an array
            dframe.sort_values(by=[DATE_COLUMN_NAME], inplace=True, ascending=False, ignore_index=True)
            response = dataframe_to_json(dframe)
    else:
        response = None
    return JSONResponse(content=response, status_code=200)


def get_float_list():
    """Loop on data directory to ges all filnames."""
    floatlist = []
    for (_, _, filenames) in walk(FLOAT_DATA_DIR):
        floatlist.extend(path.splitext(name)[0] for name in filenames)
        break
    sort = sorted(floatlist)
    return sort


def dataframe_to_json(dframe: DataFrame):
    """Convert dataframe to json."""
    result = dframe.to_json()
    json_content = json.loads(result)
    return json.dumps(json_content)


def dataframe_to_geojson(filename: str, dframe: DataFrame):
    """Convert dataframe to geojson (draws points connected by lines)."""
    features = []
    # create linestring feature
    array_of_points = list(zip(dframe[LON_COLUMN_NAME].values, dframe[LAT_COLUMN_NAME].values))
    linestring_feature = Feature(
        geometry=LineString(coordinates=array_of_points),
        properties={}
    )
    features.append(linestring_feature)
    # create points features
    points_features = dframe.apply(
        lambda row:
        Feature(
            geometry=Point((row[LON_COLUMN_NAME], row[LAT_COLUMN_NAME])),
            properties=dict(
                filename=filename,
                wmo=row[WMO_COLUMN_NAME],
                time=row[DATE_COLUMN_NAME],
                lat=row[4],
                lon=row[5],
                last=row.name == len(dframe) - 1,
                tooltip=f"<b>Wmo</b> : {row[WMO_COLUMN_NAME]} <br/><b>Date</b> : {row[DATE_COLUMN_NAME]}",
            ),
        ),
        axis=1).tolist()
    features.extend(points_features)
    # create whole geojson object
    feature_collection = FeatureCollection(features=features)
    return feature_collection


def concat_dataframe_to_geojson(dframe: DataFrame):
    """Convert concat dataframe to geojson.

    Concat dataframe correspond to latest point of each floats so we do not want to display it
    as classic dataset, here we just draw points.
    """
    features = dframe.apply(
        lambda row:
        Feature(
            geometry=Point((row[LON_COLUMN_NAME], row[LAT_COLUMN_NAME])),
            properties=dict(
                filename=row["FILENAME"],
                wmo=row[WMO_COLUMN_NAME],
                time=row[DATE_COLUMN_NAME],
                lat=row[4],
                lon=row[5],
                last=True,
                tooltip=f"<b>Wmo</b> : {row[WMO_COLUMN_NAME]} <br/><b>Date</b> : {row[DATE_COLUMN_NAME]}",
            ),
        ),
        axis=1).tolist()
    # whole geojson object
    feature_collection = FeatureCollection(features=features)
    return feature_collection
