"""Main web service implementation, this web service declare all others."""
from fastapi import FastAPI
from fastapi.security import OAuth2PasswordBearer
from environment.settings import APP_DEBUG

app = FastAPI(debug=APP_DEBUG)
oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")


@app.get("/routes")
async def read_main():
    """Get app route."""
    return {
        "routes": [
            {"method": "GET", "path": "/", "summary": "Landing"},
            {"method": "GET", "path": "/docs", "summary": "API documentation"},
            {"method": "GET", "path": "/status", "summary": "App status"},
            {"method": "GET", "path": "/recovery", "summary": "Argo float recovery dashboard"},
        ]
    }


@app.get("/status")
async def get_status():
    """Get server status."""
    return {"status": "ok"}

# Services initialisation
import application.services.login_service  # noqa: F401,E402 pylint: disable=C0413,W0611
import application.services.float_service  # noqa: F401,E402 pylint: disable=C0413,W0611
import application.services.vessel_service  # noqa: F401,E402 pylint: disable=C0413,W0611
