"""boat services."""
from datetime import datetime
import json
import requests
from fastapi import Depends, HTTPException
from fastapi.responses import JSONResponse
from geojson import Feature, FeatureCollection, Point, LineString
from application.model.api.geojson.geomar_geojson import GeomarFeature
from application.model.api.geojson.ifremer_geojson import IfremerFeatureCollection
from application.model.api.geojson.vessel_geojson import VesselFeature, VesselFeatureCollection
from application.model.api.geomar_vessel import GeomarVessel
from application.model.api.ifremer_vessel import IfremerVessel
from application.model.api.ifremer_location import IfremerLocation
from application.model.api.vtexplorer_vessel import VTExplorerVessel
from application.model.enums import VesselType
from application.model.user import User
from application.model.vessel import Vessel
from application.model.vessel_location import VesselLocation
from application.services.login_service import get_current_active_user
from application.services.shared_service import app
from application.utils.shared.classes import JSONAppEncoder
from application.utils.shared.constants import DATE_QUERY_FORMAT, GEOMAR_BOAT_API_URL, \
    IFREMER_BOAT_API_URL, VTEXPLORER_API_URL, GEOMAR_BOAT_IDS
from environment.settings import get_configurations


@app.get("/get-ifr-vessel-list", response_class=JSONResponse)
async def get_json_ifr_vessel_list():
    """Ifremer french boat list json response."""
    boatlist = get_ifr_vessel_list()
    return JSONResponse(content=boatlist, status_code=200)


@app.get("/get-geomar-vessel-list", response_class=JSONResponse)
async def get_json_geomar_vessel_list():
    """Geomar german noat list json response."""
    boatlist = get_geomar_vessel_list()
    return JSONResponse(content=boatlist, status_code=200)


@app.get("/get-ifr-vessel/{boat_id}", response_class=JSONResponse)
async def get_json_ifr_vessel_details(boat_id: str):
    """Ifremer french boat details json response."""
    boat = get_ifr_vessel_details(boat_id)
    return JSONResponse(content=boat, status_code=200)


@app.get("/get-geomar-vessel/{boat_id}", response_class=JSONResponse)
async def get_geomar_ifr_vessel_details(boat_id: str):
    """Geomar german boat details json response."""
    boat = get_geomar_vessel_details(boat_id)
    return JSONResponse(content=boat, status_code=200)


@app.get("/get-vt-imo-list", response_class=JSONResponse)
async def get_json_imo_list():
    """IMO list json response."""
    boatlist = get_vt_vessel_imo_list()
    return JSONResponse(content=boatlist, status_code=200)

@app.get("/get-vt-mmsi-list", response_class=JSONResponse)
async def get_json_mmsi_list():
    """MMSI list json response."""
    boatlist = get_vt_vessel_mmsi_list()
    return JSONResponse(content=boatlist, status_code=200)

@app.get("/get-all-vessels-positions", response_class=JSONResponse)
async def get_all_vessels_locations():  # pylint: disable=W0621,W0622
    """Get Ifremer boats locations."""
    features = []
    feature_collection = get_all_ifremer_vessel_geojson()
    features.append(feature_collection)
    boat_list = get_geomar_vessel_list()
    for boat in boat_list:
        if boat.id is not None:
            boat_feature = get_geomar_vessel_geojson(boat)
            features.append(boat_feature)
    if features:
        response = FeatureCollection(features=features)
    else:
        response = None
    return JSONResponse(content=response, status_code=200)


@app.get("/get-ifr-vessels-positions", response_class=JSONResponse)
async def get_ifr_vessels_last_locations():  # pylint: disable=W0621,W0622
    """Get all Ifremer boats last locations."""
    feature_collection = get_all_ifremer_vessel_geojson()
    return JSONResponse(content=feature_collection, status_code=200)


@app.get("/get-geomar-vessels-positions", response_class=JSONResponse)
async def get_geomar_vessels_locations(start_date: datetime = None, end_date: datetime = None):  # pylint: disable=W0621,W0622
    """Get all geomar vessels locations."""
    boat_list = get_geomar_vessel_list()
    features = []
    for boat in boat_list:
        if boat.id is not None:
            boat_features = get_geomar_vessel_geojson(boat, start_date, end_date)
            features.append(boat_features)
    if features:
        response = FeatureCollection(features=features)
    else:
        response = None
    return JSONResponse(content=response, status_code=200)


@app.get("/get-ifr-vessel-positions/{boat_id}", response_class=JSONResponse)
async def get_ifr_vessel_locations(boat_id: str, start_date: datetime = None, end_date: datetime = None):  # pylint: disable=W0621,W0622
    """Get ifremer vessel locations."""
    boat = get_ifr_vessel_details(boat_id)
    if boat is not None:
        response = get_ifremer_vessel_geojson(boat, start_date, end_date)
    else:
        response = None
    return JSONResponse(content=response, status_code=200)


@app.get("/get-geomar-vessel-positions/{boat_id}", response_class=JSONResponse)
async def get_geomar_vessel_locations(boat_id: str, start_date: datetime = None, end_date: datetime = None):  # pylint: disable=W0621,W0622
    """Get geomar vessel locations."""
    boat = get_geomar_vessel_details(boat_id)
    if boat is not None:
        response = get_geomar_vessel_geojson(boat, start_date, end_date)
    else:
        response = None
    return JSONResponse(content=response, status_code=200)


@app.get("/get-vessel-positions/{boat_id}", response_class=JSONResponse)
async def get_vessel_locations(boat_id: str, boat_type: str, start_date: datetime = None, end_date: datetime = None):  # pylint: disable=W0621,W0622
    """Get boat locations."""
    if boat_type == VesselType.IFREMER.value:
        boat = get_ifr_vessel_details(boat_id)
        if boat is not None:
            response = get_ifremer_vessel_geojson(boat, start_date, end_date)
        else:
            response = None
        return JSONResponse(content=response, status_code=200)
    if boat_type == VesselType.GEOMAR.value:
        boat = get_geomar_vessel_details(boat_id)
        if boat is not None:
            response = get_geomar_vessel_geojson(boat, start_date, end_date)
        else:
            response = None
        return JSONResponse(content=response, status_code=200)
    return None


@app.get("/get-vt-vessel-latest/imo/{imo_list}/mmsi/{mmsi_list}", response_class=JSONResponse)
async def get_vt_vessel_locations(
    imo_list: str,
    mmsi_list: str,
    output_format: str = "geojson",
    current_user: User = Depends(get_current_active_user)  # pylint: disable=W0613
):
    """Get boat vessel tracking API boat location from IMO."""
    if imo_list or mmsi_list:
        response = get_vtexplorer_vessel_geojson(imo_list, mmsi_list, output_format)
    else:
        response = None
    return JSONResponse(content=response, status_code=200)


def get_ifr_vessel_list() -> list[IfremerVessel]:
    """Requets Ifremer french boat list."""
    json_response=[]
    try:
        response = requests.get(IFREMER_BOAT_API_URL + "/vessels")
        json_response = response.json()
    except requests.exceptions.RequestException as error:
        print("[Error] : " + str(error))
    boat_list = []
    for vessel in json_response:
        boat = IfremerVessel(**vessel)
        boat_list.append(boat)
    return boat_list


def get_geomar_vessel_list() -> list[GeomarVessel]:
    """Requets geomar german boat list."""
    json_response=[]
    try:
        response = requests.get(GEOMAR_BOAT_API_URL + "/platforms")
        json_response = response.json()
    except requests.exceptions.RequestException as error:
        print("[Error] : " + str(error))
    boat_list = []
    for vessel in json_response:
        boat = GeomarVessel(**vessel)
        if boat.shortname in GEOMAR_BOAT_IDS:
            boat_list.append(boat)
    return boat_list


def get_ifr_vessel_details(boat_id: str) -> IfremerVessel:
    """Requets Ifremer boat details."""
    try:
        response = requests.get(IFREMER_BOAT_API_URL + f"/vessels/{boat_id}")
        json_response = response.json()
    except requests.exceptions.RequestException as error:
        print("[Error] : " + str(error))
    boat = IfremerVessel(**json_response)
    return boat


def get_geomar_vessel_details(boat_id: str) -> GeomarVessel:
    """Requets geomar german boat details."""
    json_response=[]
    try:
        response = requests.get(GEOMAR_BOAT_API_URL + "/platforms")
        json_response = response.json()
    except requests.exceptions.RequestException as error:
        print("[Error] : " + str(error))
    for vessel in json_response:
        boat = GeomarVessel(**vessel)
        if boat.shortname == boat_id:
            return boat
    return None


def get_vt_vessel_imo_list():
    """Requets vessel tracking imo list."""
    config = get_configurations()
    if 'vessels' in config:
        return config['vessels']['imo_list']
    return []


def get_vt_vessel_mmsi_list():
    """Requets vessel tracking mmsi list."""
    config = get_configurations()
    if 'vessels' in config:
        return config['vessels']['mmsi_list']
    return []

def get_all_ifremer_vessel_geojson():  # pylint: disable=W0622
    """Requets ifremer vessel locations between two dates."""
    # create query
    response = requests.get(IFREMER_BOAT_API_URL + "/positions", timeout=10)
    # convert response
    json_response = response.json()
    ifr_feature_collection = IfremerFeatureCollection(**json_response)
    feature_collection = VesselFeatureCollection(ifr_feature_collection)
    if not feature_collection:
        return None
    return json.loads(JSONAppEncoder().encode(feature_collection))


def get_ifremer_vessel_geojson(vessel: IfremerVessel, start_date: datetime, end_date: datetime):  # pylint: disable=W0622
    """Requets ifremer vessel locations between two dates."""
    # create query
    start_date_attribut = end_date_attribut = ""
    if start_date is not None:
        start_date_time = start_date.strftime(DATE_QUERY_FORMAT)
        start_date_attribut = f"?startDate={start_date_time}"
        if end_date is not None:
            end_date_time = end_date.strftime(DATE_QUERY_FORMAT)
            end_date_attribut = f"&endDate={end_date_time}"
    response = requests.get(IFREMER_BOAT_API_URL +
                            f"/vessels/{vessel.id}/positions" +
                            start_date_attribut + end_date_attribut
                            )
    # convert response
    json_response = response.json()
    if not json_response:
        return None
    location_list = []
    vessel = Vessel(vessel)
    for location in json_response:
        vessel_location = VesselLocation(IfremerLocation(**location))
        location_list.append(vessel_location)
    feature_collection = create_feature_collection_from_vessel_locations(vessel, location_list)
    return json.loads(JSONAppEncoder().encode(feature_collection))


def get_geomar_vessel_geojson(boat: GeomarVessel, start_date: datetime = None, end_date: datetime = None):  # pylint: disable=W0622,W0613
    """Requets geomar vessel locations between two dates."""
    # create query, keep start_date and end_date waiting to know if we can get location between 2 dates with the API.
    if start_date is not None:
        start_date = start_date.strftime("%Y-%m-%d")
        start_date_attribut = f"&dateFrom={start_date}"
        if end_date is not None:
            end_date_time = end_date.strftime("%Y-%m-%d")
            end_date_attribut = f"&dateTo={end_date_time}"

    # convert geomar vessel to a genenic boat
    vessel = Vessel(boat)
    if start_date is None:
        query = GEOMAR_BOAT_API_URL + f"/platforms/currentloc?id={boat.shortname}"
        return get_geojson_generic_vessel_feature(query, vessel)
    query = GEOMAR_BOAT_API_URL + "/tracks/postGisTrackByDate?" + \
        f"platform={boat.shortname}{start_date_attribut}{end_date_attribut}"
    return get_geojson_generic_vessel_feature_collection(query, vessel)


def get_vtexplorer_vessel_geojson(imo_list: str, mmsi_list: str, format: str = "json"):  # pylint: disable=W0622
    """Requets vt rexplorer vessel list from imo or/and mmsi."""
    config = get_configurations()
    imo_attribut = ""
    mmsi_attribut = ""
    if imo_list and imo_list != 'None':
        imo_attribut = f"&imo={imo_list}"
    
    if mmsi_list and mmsi_list != 'None':
        mmsi_attribut = f"&mmsi={mmsi_list}"
    
    if imo_attribut or mmsi_attribut:
        response = requests.get(VTEXPLORER_API_URL + "/vessels"
                                f"?userkey={config['vessels']['api_key']}&sat=1" + imo_attribut + mmsi_attribut)
        json_response = response.json()
    else:
        json_response = []
    
    if 'error' in json_response:
        raise HTTPException(status_code=401, detail=json_response['error'])
    if format.lower() == 'json':
        return json_response
    
    features = []
    # create boat location features
    for vt_vessel in json_response:
        vt_explorer_response = VTExplorerVessel(**vt_vessel['AIS'])
        vessel = Vessel(vt_explorer_response)
        vessel_location = VesselLocation(vt_explorer_response)
        features.append(create_feature_from_vessel_location(vessel, vessel_location))

    feature_collection = FeatureCollection(features=features)

    return json.loads(JSONAppEncoder().encode(feature_collection))


def get_geojson_generic_vessel_feature(query: str, vessel: Vessel):  # pylint: disable=W0622,W0613
    """Requets generic endpoint that return GeoJson Data."""
    # create query, keep start_date and end_date waiting to know if we can get location between 2 dates with the API.
    response = requests.get(query)
    # convert response
    json_response = response.json()

    if vessel.type is VesselType.GEOMAR.value:
        api_feature = GeomarFeature(**json_response)
    feature = VesselFeature(api_feature)
    # add attributs not set in the generic APIs but required from model
    feature.properties.vessel = vessel
    return json.loads(JSONAppEncoder().encode(feature))


def get_geojson_generic_vessel_feature_collection(query: str, vessel: Vessel):  # pylint: disable=W0622,W0613
    """Requets generic endpoint that return GeoJson Data."""
    # create query, keep start_date and end_date waiting to know if we can get location between 2 dates with the API.
    response = requests.get(query)
    # convert response
    json_response = response.json()

    if vessel.type is VesselType.GEOMAR.value:
        api_feature = GeomarFeature(**json_response)
    feature = VesselFeature(api_feature)
    # add attributs not set in the generic APIs but required from model
    feature.properties.vessel = vessel
    feature_collection = FeatureCollection(features=[feature])
    return json.loads(JSONAppEncoder().encode(feature_collection))


def create_feature_collection_from_vessel_locations(vessel: Vessel, vessel_locations: list[VesselLocation]):
    """Create feature from ifremer vessel location API (points and linestring)."""
    linestring_coordinates = []
    points_features = []
    for vessel_location in vessel_locations:
        linestring_coordinates.append([vessel_location.lon, vessel_location.lat])
        feature = create_feature_from_vessel_location(vessel, vessel_location)
        points_features.append(feature)
    linestring_feature = Feature(
        geometry=LineString(coordinates=linestring_coordinates),
        properties={}
    )
    features = [linestring_feature, *points_features]
    return FeatureCollection(features=features)


def create_feature_from_vessel_location(vessel: Vessel, location: VesselLocation):
    """Create geojson feature from boat location."""
    properties = dict(
        vessel=vessel,
        data=location.data,
        date=location.date,
        tooltip=f"<b>Wmo</b> : {vessel.name} <br/><b>Date</b> : {location.date}",
    )

    return Feature(
        geometry=Point((location.lon, location.lat)),
        properties=properties,
    )
