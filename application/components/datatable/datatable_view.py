"""Datatable view."""
from dash import html, dash_table
import pandas as pd

# Container for content tab
datatable_view = html.Div(
    id="datatable-content",
    className="p-2 p-lg-3"
)


def make_datatable(float_locations):
    """Container for datatable."""
    # Init datatable
    dframe = pd.read_json(float_locations)
    datatable = html.Div([
        dash_table.DataTable(
            id="locations-table",
            columns=[{"name": i, "id": i} for i in dframe.columns],
            data=dframe.to_dict('records'),
            style_table={'overflowX': 'auto', 'height': '85vh'},
            style_cell=dict(textAlign='left'),
            export_format="csv",
        )
    ])

    return datatable
