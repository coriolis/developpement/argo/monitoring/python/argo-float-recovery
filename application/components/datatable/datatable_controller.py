"""Datatable controller."""
import requests
from dash import html
from dash.dependencies import Input, Output
import dash_bootstrap_components as dbc
from environment.settings import SERVER_URL
from main import dash_app
from application.components.datatable import datatable_view


@dash_app.callback(
    Output('datatable-content', 'children'),
    [
        Input('dropdownFloats', 'value'),
        Input('schedule-component', 'n_intervals')
    ]
)
def make_content_datatable(float_wmo, n_intervals):  # pylint: disable=W0613
    """Callback for make content datatable every minutes (from schedule-component) or when float filter change."""  # pylint: disable=C0301
    if float_wmo is not None:
        # if the store contains an observation
        float_locations_request = requests.get(SERVER_URL + "get-float/" + float_wmo + "?output_format=json")
        datatable_float_locations = float_locations_request.json()
        if datatable_float_locations:
            content = datatable_view.make_datatable(datatable_float_locations)
            return content
    else:
        floats_latest_locations_request = requests.get(SERVER_URL + "get-floats-latest/?output_format=json")
        floats_latest_locations = floats_latest_locations_request.json()
        if floats_latest_locations:
            content = datatable_view.make_datatable(floats_latest_locations)
            return content
    return html.Div(
        dbc.Container(
            [
                html.H2(f"Error : {floats_latest_locations['file_error'][0]}", className="text-danger"),
                html.Hr(className="my-2"),
                html.P(f"{floats_latest_locations['file_error'][1]}"),
            ],
            fluid=True,
            className="py-3",
        ),
        className="p-3 bg-light rounded-3",
    )
