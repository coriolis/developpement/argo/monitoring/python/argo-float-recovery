"""Map popup component."""
from datetime import datetime
from dash import html, dcc
import dash_bootstrap_components as dbc

from application.model.enums import VesselType
from application.utils.shared.constants import DATE_FEATURE_FORMAT, DATE_POPUP_FORMAT


def make_float_popup(properties):
    """Make custom component map popup."""
    hasattr(properties, 'filename')
    return html.Div(
        id="map-popup",
        children=[
            html.H5(
                [
                    "Platform ",
                    html.A(
                        [
                            properties['wmo'],
                            html.I(className="fa fa-external-link", style={"marginLeft": "2px"})
                        ],
                        href=f"https://fleetmonitoring.euro-argo.eu/float/{properties['wmo']}",
                        target="_blank"
                    )
                ],
                className="text-info",
                style={
                    "fontSize": "1rem",
                    "fontWeight": "bold",
                    "textTransform": "uppercase",
                }
            ),
            html.Div(
                children=[
                    html.Button(
                        "Download data",
                        id="btn-download-data",
                        className="btn btn-link",
                        title="download data file in CSV format",
                        value=properties['filename'],
                        style={
                            "fontWeight": "600",
                            "color": "#000",
                        },
                    ),
                    dcc.Download(id="download-data")
                ] if 'filename' in properties and properties['last'] else [],
                className="text-center",
            ),
            html.Div(className="d-flex justify-content-between"),
            html.Table(
                [
                    html.Tr(
                        [
                            html.Th("Date", style={"paddingTop": ".25rem"}),
                            html.Th("Position", style={"paddingTop": ".25rem"})
                        ]
                    ),
                    html.Tr(
                        [
                            html.Td(f"{properties['time']}"),
                            html.Td(
                                [
                                    html.P(f"{properties['lon']}", className="m-0"),
                                    html.P(f"{properties['lat']}", className="m-0")
                                ]
                            )
                        ]
                    ),
                ],
                style={
                    "width": "100%",
                    "borderTop": "1px solid #F1F1F1",
                }
            )
        ]
    )


def make_vessel_popup(feature):
    """Make custom component map popup."""
    if ('type' in feature['properties']['vessel'] and
            feature['properties']['vessel']['type'] == VesselType.IFREMER.value):
        return make_harmonized_vessel_popup(feature)
    return make_generic_vessel_popup(feature)


def make_harmonized_vessel_popup(feature):
    """Make custom component map popup."""
    properties = feature['properties']
    geometry = feature['geometry']
    boat_link = html.A(
        [
            html.I(className="fa fa-external-link", style={"marginLeft": "2px"})
        ],
        href=f"{properties['vessel']['url']}",
        target="_blank"
    ) if 'url' in properties else None

    return html.Div(
        id="map-popup",
        children=[
            html.H5(
                [
                    properties['vessel']['name'],
                    boat_link,
                ],
                className="text-secondary",
                style={
                    "fontSize": "1rem",
                    "fontWeight": "bold",
                    "textTransform": "uppercase",
                }
            ),
            html.Div(className="d-flex justify-content-between"),
            create_properties_table(properties, geometry)
        ]
    )


def create_properties_table(properties, geometry):
    """Make custom component table popup."""
    additional_rows = []
    data = properties["data"]
    if data:
        if 'heading' in data and 'speed' in data:
            additional_rows.append(
                html.Tr(
                    [
                        html.Th("Heading", style={"paddingTop": ".25rem"}),
                        html.Th("Speed", style={"paddingTop": ".25rem"})
                    ]
                )
            )
            additional_rows.append(
                html.Tr(
                    [
                        html.Td(f"{data['heading']}°"),
                        html.Td(f"{data['speed']} knots"),
                    ]
                )
            )

        if 'truewinddir' in data and 'truewindspeed' in data:
            additional_rows.append(
                html.Tr(
                    [
                        html.Th("Wind direction", style={"paddingTop": ".25rem"}),
                        html.Th("Wind speed", style={"paddingTop": ".25rem"})
                    ]
                )
            )
            additional_rows.append(
                html.Tr(
                    [
                        html.Td(f"{data['truewinddir']}°"),
                        html.Td(f"{data['truewindspeed']} knots"),
                    ]
                )
            )

        if 'seatemp' in data and 'airtemp' in data:
            additional_rows.append(
                html.Tr(
                    [
                        html.Th("Sea temperature", style={"paddingTop": ".25rem"}),
                        html.Th("Air temperature", style={"paddingTop": ".25rem"})
                    ]
                )
            )
            additional_rows.append(
                html.Tr(
                    [
                        html.Td(f"{data['seatemp']} °C"),
                        html.Td(f"{data['airtemp']} °C"),
                    ]
                )
            )

    rows = [
        html.Tr(
            [
                html.Th("Date", style={"paddingTop": ".25rem"}),
                html.Th("Position", style={"paddingTop": ".25rem"})
            ]
        ),
        html.Tr(
            [
                html.Td(f"{datetime.strptime(properties['date'], DATE_FEATURE_FORMAT).strftime(DATE_POPUP_FORMAT)}"),
                html.Td(
                    [
                        html.P(f"{geometry['coordinates'][0]}", className="m-0"),
                        html.P(f"{geometry['coordinates'][1]}", className="m-0")
                    ]
                )
            ]
        ),
        *additional_rows
    ]

    table = html.Table(
        rows,
        style={
            "width": "100%",
            "borderTop": "1px solid #F1F1F1",
        }
    )

    return table


def make_generic_vessel_popup(feature):
    """Make a generic popup for various GeoJson APIs."""
    properties = feature['properties']
    geometry = feature['geometry']
    boat_link = html.A(
        [
            html.I(className="fa fa-external-link", style={"marginLeft": "2px"})
        ],
        href=f"{properties['vessel']['url']}",
        target="_blank"
    ) if 'url' in properties['vessel'] else None

    return html.Div(
        id="map-popup",
        children=[
            html.H5(
                [
                    properties['vessel']['name'],
                    boat_link,
                ],
                className="text-secondary",
                style={
                    "fontSize": "1rem",
                    "fontWeight": "bold",
                    "textTransform": "uppercase",
                }
            ),
            html.Div(className="d-flex justify-content-between"),
            create_generic_properties_table(properties, geometry)
        ]
    )


def create_generic_properties_table(properties, geometry):
    """Make a div for generic geojon properties."""
    cols = []

    # date
    if properties['date'] is not None:
        cols.append(
            dbc.Col(
                html.Div([
                    html.Strong('date'),
                    html.P(
                        f"{datetime.strptime(properties['date'], DATE_FEATURE_FORMAT).strftime(DATE_POPUP_FORMAT)}",
                        className="my-2"
                    )
                ]),
                width=6)
        )

    # coordinates
    if geometry['coordinates'] is not None and len(geometry['coordinates']) == 2:
        cols.append(
            dbc.Col(
                html.Div([
                    html.Strong('position'),
                    html.P(f"{geometry['coordinates'][0]}", className="m-0 mt-1"),
                    html.P(f"{geometry['coordinates'][1]}", className="m-0 mb-1")
                ]),
                width=6)
        )

    # generic properties (filtering some attributs)
    data = properties['data']
    for key in data:
        if data[key] is not None:
            cols.append(
                dbc.Col(
                    html.Div([
                        html.Strong(key),
                        html.P(data[key], className="my-1 text-truncate")
                    ]),
                    width=6)
            )

    return html.Div(
        dbc.Row(cols),
        className="pt-1",
        style={
            "width": "100%",
            "borderTop": "1px solid #F1F1F1",
        }
    )
