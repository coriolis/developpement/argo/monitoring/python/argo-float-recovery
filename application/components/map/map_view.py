"""Map view."""
from dash import html
import dash_leaflet as dl
from dash_extensions.javascript import Namespace
import requests

from environment.settings import SERVER_URL

ns = Namespace("leaflet", "geojsonStyle")


def request_float_wmo_geojson(float_wmo):
    """Get geojson locations from get-float endpoint."""
    float_request = requests.get(SERVER_URL + "get-float/" + float_wmo + "?output_format=geojson")
    geojson_locations = float_request.json()
    if geojson_locations:
        return geojson_locations
    return request_floats_geojson()


def request_floats_geojson():
    """Get geojson locations from get-floats-latest endpoint."""
    floats_request = requests.get(SERVER_URL + "get-floats-latest/?output_format=geojson")
    geojson_locations = floats_request.json()
    if geojson_locations:
        return geojson_locations
    return None


def float_locations(float_wmo: str = None):
    """Initialize geojson floats locations."""
    if float_wmo is not None:
        geojson = request_float_wmo_geojson(float_wmo)
    else:
        geojson = request_floats_geojson()
    return dl.GeoJSON(
        id="float-locations",
        options=dict(pointToLayer=ns("floatFeatureToLayer"), ),
        zoomToBounds=True,  # when true, zooms to bounds when data changes (e.g. on load)
        zoomToBoundsOnClick=True,  # when true, zooms to bounds of feature (e.g. polygon) on click
        children=[dl.Popup(id="float-popup-content")],
        data=geojson
    )


ifr_boats_locations = dl.GeoJSON(
    id="ifr-boats-latest",
    options=dict(pointToLayer=ns("boatLastestFeatureToLayer")),  # style=ns("boatLineStyle")
    children=[dl.Popup(id="ifr-boat-popup-content")]
)

selected_boat_location = dl.GeoJSON(
    id="selected-boat-latest",
    options=dict(pointToLayer=ns("boatLastestFeatureToLayer")),  # style=ns("boatLineStyle")
    children=[dl.Popup(id="selected-boat-popup-content")]
)

clicked_boat_locations = dl.GeoJSON(
    id="clicked-boat-locations",
    options=dict(pointToLayer=ns("boatFeatureToLayer")),  # style=ns("boatLineStyle")
    children=[dl.Popup(id="clicked-boat-popup-content")]
)


def dl_map(float_wmo: str = None):
    """Initialize map view."""
    return dl.MapContainer(
        children=[
            dl.LayersControl(
                [
                    dl.BaseLayer(
                        dl.TileLayer(),
                        name="OpenStreetMap",
                        checked=True
                    ),
                    dl.BaseLayer(
                        dl.TileLayer(
                            url="https://tiles.emodnet-bathymetry.eu/2020/baselayer/web_mercator/{z}/{x}/{y}.png",
                            attribution='''Global land and water coverage &copy;
                            <a target='_blank' href='https://www.emodnet-bathymetry.eu/'>emodnet-bathymetry</a>''',
                            tms=False
                        ),
                        name="EMODnet Bathymetry"
                    ),
                    dl.BaseLayer(
                        dl.TileLayer(
                            url='''https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/
                            tile/{z}/{y}/{x}''',
                            attribution="World Imagery Tiles &copy; Esri "
                        ),
                        name="Satellite"
                    )
                ]
            ),
            dl.LocateControl(locateOptions={'enableHighAccuracy': True}),
            dl.MeasureControl(
                position="topleft",
                primaryLengthUnit="kilometers",
                primaryAreaUnit="hectares",
                activeColor="#214097",
                completedColor="#972158"
            ),
            dl.FeatureGroup([
                dl.EditControl(id="edit_control")
            ]),
            # dl.EditControl(id="edit_control"),
            float_locations(float_wmo),
            ifr_boats_locations,
            selected_boat_location,
            clicked_boat_locations,
        ],
        id="map",
        center=[0,0], 
        zoom=6,
        style={'height': 'calc(100vh - 6vh)', 'margin': "auto", "display": "block"},
    )


def map_view(float_wmo: str = None):
    """Initialize map view."""
    return html.Div(id='map-content', children=[
        dl_map(float_wmo),
        html.Div(
            id='coordinate-hover-id',
            style={
                "position": "absolute",
                "bottom": 0,
                "zIndex": 500
            }
        )
    ])
