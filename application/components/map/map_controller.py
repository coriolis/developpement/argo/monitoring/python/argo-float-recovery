"""Map controller."""
from datetime import datetime, timedelta
import dash
import requests
import numpy as np
from dash.dependencies import Input, Output, State
from dash import dcc
from environment.settings import FLOAT_DATA_DIR, SERVER_URL
from application.components.map.map_view import request_float_wmo_geojson, request_floats_geojson
from application.pages.dashboard_controller import HaltCallback
from application.utils.shared.constants import DATE_FEATURE_FORMAT, DATE_QUERY_FORMAT
from application.components.map.map_popup import make_float_popup, make_vessel_popup
from application.utils.shared.functions import get_triggered_id
from main import dash_app


@dash_app.callback(
    Output('coordinate-hover-id', 'children'),
    [Input('map', 'clickData')]
)
def click_coord(coordinates):
    """Share coordinates on click."""
    if coordinates is not None:
        array = np.array([coordinates['latlng']['lat'], coordinates['latlng']['lng']]).round(decimals=4)
        return np.array_str(array)
    return ""


@dash_app.callback(
    [
        Output('float-locations', 'data'),
        Output('float-locations', 'zoomToBounds')
    ],
    [
        Input('dropdownFloats', 'value'),
        Input('schedule-component', 'n_intervals')
    ]
)
def update_float_geojson_data(float_wmo, n_intervals):
    """Update map geojson data and zoomToBounds attributs every minutes (from schedule-component) or when float filter change."""  # pylint: disable=C0301
    input_id = get_triggered_id(dash.callback_context)

    if float_wmo is not None:
        geojson = request_float_wmo_geojson(float_wmo)
        return [geojson, input_id == "dropdownFloats"]

    geojson = request_floats_geojson()
    return [geojson, n_intervals == 0 or input_id == "dropdownFloats"]


@dash_app.callback(
    Output("float-popup-content", "children"),
    [
        Input("float-locations", "clickData"),
        Input("dropdownFloats", "on_float_change")
    ]
)
def float_location_click(feature, on_float_change):  # pylint: disable=W0613
    """Callback which populate or unpopulate custom popup."""
    input_id = get_triggered_id(dash.callback_context)

    if input_id == "float-locations":
        if feature is not None:
            return make_float_popup(feature['properties'])
    return None


@dash_app.callback(
    Output('selected-boat-latest', 'data'),
    Input('dropdownImo', 'value'),
    Input('dropdownMmsi', 'value'),
    State("authorization", "value"),
)
def update_selected_vessel_geojson_data(imo_list, mmsi_list, authorization):
    """Update map boat latest data when boat filter change."""
    if imo_list is not None or mmsi_list is not None :
        return request_vt_latest_location_geojson(imo_list, mmsi_list, authorization)

    return None


@dash_app.callback(
    Output('ifr-boats-latest', 'data'),
    [
        Input('schedule-component', 'n_intervals'),
    ]
)
def update_ifr_boats_geojson_data(n_intervals):  # pylint: disable=W0613
    """Update map boat latest data when boat filter change."""
    if n_intervals is not None:
        return request_all_vessels_locations_geojson()
    return None


@dash_app.callback(
    Output('clicked-boat-locations', 'data'),
    [
        Input("ifr-boats-latest", "clickData"),
        Input("dropdownImo", "value"),
        Input("dropdownMmsi", "value"),
    ]
)
def update_clicked_vessel_geojson_data(feature, on_imo_change, on_mmsi_change):  # pylint: disable=W0613
    """Update map boat locations from date to date when clic on latest location."""
    input_id = get_triggered_id(dash.callback_context)

    if input_id == "dropdownImo" or input_id == "dropdownMmsi":
        return None

    if feature is not None:
        feature_date = datetime.strptime(feature['properties']['date'], DATE_FEATURE_FORMAT)
        return request_vessel_locations_geojson(
            feature['properties']['vessel']['id'],
            feature['properties']['vessel']['type'],
            feature_date
        )
    return None


@dash_app.callback(
    Output("selected-boat-popup-content", "children"),
    Input("selected-boat-latest", "clickData"),
)
def update_selected_vessel_popup(feature):
    """Update map boat latest popup content."""
    if feature is not None:
        return make_vessel_popup(feature)
    return None


@dash_app.callback(
    Output("ifr-boat-popup-content", "children"),
    Input("ifr-boats-latest", "clickData")
)
def update_ifr_vessel_popup(feature):
    """Update map boat latest popup content."""
    if feature is not None:
        return make_vessel_popup(feature)
    return None


@dash_app.callback(
    Output("clicked-boat-popup-content", "children"),
    Input("clicked-boat-locations", "clickData"),
)
def update_clicked_vessel_popup(feature):
    """Update map boat popup content."""
    if feature is not None:
        return make_vessel_popup(feature)
    return None


@dash_app.callback(
    Output("download-data", "data"),
    Input("btn-download-data", "n_clicks"),
    State('btn-download-data', 'value'),
    prevent_initial_call=True,
)
def download_data_file(n_clicks, value):  # pylint: disable=W0613
    """Callback which download data file."""
    return dcc.send_file(
        FLOAT_DATA_DIR + '/' + value + '.csv'
    )


#################################################
#          CONTROLLER FUNCTIONS                 #
#################################################
def request_all_vessels_locations_geojson():
    """Get geojson latest location from all boats."""
    boats_request = requests.get(SERVER_URL + "get-all-vessels-positions")
    geojson_locations = boats_request.json()
    if "detail" in geojson_locations:
        raise HaltCallback(geojson_locations['detail'])
    if geojson_locations:
        return geojson_locations
    return None


def request_all_geomar_vessels_locations_geojson():
    """Get geojson latest location from all boats."""
    boats_request = requests.get(SERVER_URL + "get-geomar-vessels-positions")
    geojson_locations = boats_request.json()
    if "detail" in geojson_locations:
        raise HaltCallback(geojson_locations['detail'])
    if geojson_locations:
        return geojson_locations
    return None


def request_all_ifr_vessels_locations_geojson():
    """Get geojson latest location from all boats."""
    boats_request = requests.get(SERVER_URL + "get-ifr-vessels-positions")
    geojson_locations = boats_request.json()
    if "detail" in geojson_locations:
        raise HaltCallback(geojson_locations['detail'])
    if geojson_locations:
        return geojson_locations
    return None


def request_vt_latest_location_geojson(imo_list, mmsi_list, authorization):
    """Get geojson locations from get-float endpoint."""
    header, imo_attribut, mmsi_attribut = None, None, None
    if authorization:
        header = {'Authorization': authorization}
    if imo_list:
        imo_attribut = f"{','.join(imo_list)}"
    if mmsi_list:
        mmsi_attribut = f"{','.join(mmsi_list)}"   
    boat_request = requests.get(
        SERVER_URL + f"get-vt-vessel-latest/imo/{imo_attribut}/mmsi/{mmsi_attribut}?output_format=geojson", headers=header
    )
    geojson_location = boat_request.json()
    if "detail" in geojson_location:
        raise HaltCallback(geojson_location['detail'])
    if geojson_location:
        return geojson_location
    return None


def request_vessel_locations_geojson(boat_id: str, boat_type: str, end_date: datetime):
    """Get geojson locations from get-float endpoint."""
    start_date = end_date - timedelta(days=2)
    start_date_attribut = f"&start_date={start_date.strftime(DATE_QUERY_FORMAT)}"
    end_date_attribut = f"&end_date={end_date.strftime(DATE_QUERY_FORMAT)}"
    boat_request = requests.get(SERVER_URL + f"get-vessel-positions/{boat_id}"
                                + f"?boat_type={boat_type}"
                                + start_date_attribut + end_date_attribut)
    geojson_location = boat_request.json()
    if "detail" in geojson_location:
        raise HaltCallback(geojson_location['detail'])
    if geojson_location:
        return geojson_location
    return None
