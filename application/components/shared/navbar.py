"""Application navbar component."""
from dash import html
import dash_bootstrap_components as dbc
from application.utils.shared.constants import APPLICATION_NAME
from application.components.shared.login import modal

login_bar = dbc.Row(
    [
        dbc.Col(
            modal,
            width="auto",
        ),
    ],
    className="g-0 ms-auto flex-nowrap mt-3 mt-md-0",
    align="center",
)

navbar = dbc.Navbar(
    dbc.Container(
        [
            html.A(
                # Use row and col to control vertical alignment of logo / brand
                dbc.Row(
                    [
                        dbc.Col(html.Img(src="/assets/Logo_Ifremer_RVB.png", height="30px")),
                        dbc.Col(dbc.NavbarBrand(APPLICATION_NAME, className="ms-2")),
                    ],
                    align="center",
                    className="g-0",
                ),
                # href="https://plotly.com",
                style={"textDecoration": "none"},
            ),
            dbc.NavbarToggler(id="navbar-toggler", n_clicks=0),
            dbc.Collapse(
                login_bar,
                id="navbar-collapse",
                is_open=False,
                navbar=True,
            ),
        ],
        fluid=True,
    ),
    color="#264b6d",
    dark=True,
    fixed="top",
    expand="lg",
    style={
        "height": "6vh"
    }
)
