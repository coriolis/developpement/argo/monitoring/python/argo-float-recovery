"""Sidebar controller, contains callbacks from sidebar component."""
import json
import requests
import dash
from dash.dependencies import Input, Output, State
from application.utils.shared.functions import get_triggered_id
from environment.settings import SERVER_URL
from main import dash_app


@dash_app.callback(
    Output("login-modal", "is_open"),
    [Input("login", "n_clicks"), Input("close-login", "n_clicks"), Input("authorization", "value")],
    [State("login-modal", "is_open")],
)
def toggle_modal(open_click, close_click, logged_in, is_open):
    """Callback close open login modal."""
    input_id = get_triggered_id(dash.callback_context)

    if input_id == "authorization":
        if logged_in:
            return False
        return True

    if open_click or close_click:
        return not is_open
    return is_open


@dash_app.callback(

    [
        Output("authorization", "value"),
        Output("password", "invalid"),
        Output("password-feedback", "children")
    ],
    [
        Input("login_form", "n_submit"),
        Input("logout", "n_clicks"),
    ],
    State("username", "value"),
    State("password", "value"),
    prevent_initial_call=True
)
def handle_submit(n_submit, n_logout, username, password):  # pylint: disable=W0613
    """Callback login / logout."""
    authorization = None
    message = None

    input_id = get_triggered_id(dash.callback_context)

    if input_id == "logout":
        return [None, False, None]

    param = {
        'grant_type': 'password',
        'username': username,
        'password': password
    }
    post = requests.post(SERVER_URL + 'token', data=param)
    if post.text:
        json_auth = json.loads(post.text)
        if 'access_token' in json_auth:
            authorization = json_auth['token_type'] + ' ' + json_auth['access_token']
        if 'detail' in json_auth:
            message = json_auth['detail']
    return [authorization, message is not None, message]


@dash_app.callback(
    [Output("login", "className"), Output("logout", "className")],
    [Input("authorization", "value")],
)
def toggle_login_button(authorization):
    """Callback show / hide login and logout buttons."""
    if authorization:
        return ["d-none", "d-block"]
    return ["d-block", "d-none"]
