"""sidebar component."""
from dash import html, dcc
import dash_bootstrap_components as dbc
import requests

from environment.settings import SERVER_URL, load_external_configurations


def get_float_dropdown_options():
    """Get float dropdown options."""
    float_list_request = requests.get(SERVER_URL + "get-float-list")
    float_list = float_list_request.json()
    return [{'label': float_wmo, 'value': float_wmo} for float_wmo in float_list]


def get_boat_imo_dropdown_options():
    """Get IMO dropdown options."""
    load_external_configurations()  # update settings in case dynamique settings changed
    boat_list_request = requests.get(SERVER_URL + "get-vt-imo-list")
    boat_list = boat_list_request.json()
    return [{'label': boat, 'value': boat} for boat in boat_list]

def get_boat_mmsi_dropdown_options():
    """Get MMSI dropdown options."""
    load_external_configurations()  # update settings in case dynamique settings changed
    boat_list_request = requests.get(SERVER_URL + "get-vt-mmsi-list")
    boat_list = boat_list_request.json()
    return [{'label': boat, 'value': boat} for boat in boat_list]

def float_dropdown(float_wmo: str):
    """Initialize float dropdown."""
    return dbc.Row(
        [
            dbc.Label("Floats", html_for="dropdownFloats", style={"color": "#FFFFFF", "fontWeight": "bold"}),
            dcc.Dropdown(
                id="dropdownFloats",
                options=get_float_dropdown_options(),
                value=float_wmo
            )
        ],
    )


def boat_dropdown():
    """Initialize boat dropdown."""
    return dbc.Row(
        [
            dbc.Label("Vessels", html_for="dropdownImo", style={"color": "#FFFFFF", "fontWeight": "bold"}),
            dcc.Dropdown(
                id="dropdownImo",
                multi=True,
                placeholder="Select an IMO",
                options=get_boat_imo_dropdown_options(),
            ),
            dcc.Dropdown(
                id="dropdownMmsi",
                multi=True,
                placeholder="Select an MMSI",
                options=get_boat_mmsi_dropdown_options(),
            ),
        ],
        id="vessels-filter",
        className="invisible",
    )


def sidebar(float_wmo: str):
    """Initialize side bar."""
    return dbc.Collapse(
        [
            html.H5("FILTERS", className="h5", style={"color": "#F8C844", "fontWeight": "bold"}),
            html.Hr(style={"backgroundColor": "#FFFFFF"}),
            html.Div(float_dropdown(float_wmo)),
            html.Div(boat_dropdown()),
            html.H5("DISPLAY", className="h5 mt-3", style={"color": "#F8C844", "fontWeight": "bold"}),
            html.Hr(style={"backgroundColor": "#FFFFFF"}),
            dcc.Tabs(
                id='tabs-display',
                value='tab-map',
                vertical=True,
                children=[
                    dcc.Tab(
                        label='Map',
                        value='tab-map',
                        style={
                            'borderBottom': '1px solid #d6d6d6',
                            'padding': '6px',
                        },
                        selected_style={
                            'padding': '6px',
                        }
                    ),
                    dcc.Tab(
                        label='Table',
                        value='tab-table',
                        style={
                            'borderBottom': '1px solid #d6d6d6',
                            'padding': '6px',
                        },
                        selected_style={
                            'padding': '6px',
                        }
                    ),
                ],
                style={
                    'width': '100%',
                },
                parent_style={
                    'width': '100%'
                },
            ),
            html.Div(
                html.Img(src="/assets/euro_argo_logo.png"),
                className="text-center p-3 d-none d-lg-block",
                style={
                    "position": "absolute",
                    "bottom": 0,
                    "right": 0,
                    "left": 0,
                }
            )
        ],
        id='sidebar-collapse',
        className='col-12 col-lg-2 p-3',
        navbar=False,
        is_open=True,
        style={
            "backgroundColor": "#276095",
        },
    )
