"""Sidebar controller, contains callbacks from sidebar component."""
from dash.dependencies import Input, Output, State
import dash
# from configuration import load_external_configurations
from main import dash_app
from application.utils.shared.functions import get_triggered_id
from application.components.datatable.datatable_view import datatable_view
from application.components.map.map_view import map_view
from application.components.shared.sidebar import get_float_dropdown_options


@dash_app.callback(
    Output('tabs-display-content', 'children'),
    Input('tabs-display', 'value'),
    State('dropdownFloats', 'value')
)
def make_content_display_tab(tab, float_wmo):
    """Callback that display selected tab."""
    if tab == 'tab-table':
        return datatable_view
    if tab == 'tab-map':
        return map_view(float_wmo)
    return map_view(float_wmo)


@dash_app.callback(
    Output('dropdownFloats', 'options'),
    [
        Input('schedule-component', 'n_intervals')
    ]
)
def make_content_float_dropdown(n_intervals):  # pylint: disable=W0613
    """Callback that load float filter option every minutes (from schedule-component) or when float filter change."""
    return get_float_dropdown_options()


@dash_app.callback(
    Output("vessels-filter", "className"),
    [Input("authorization", "value")],
)
def make_boat_dropdown(authorization):
    """Callback that load boat filter option every minutes at init or when boat filter change."""
    if authorization:
        return "visible"
    return "invisible"


@dash_app.callback(
    Output('dropdownFloats', 'value'),
    [
        Input("float-locations", "clickData"),
    ],
    State('dropdownFloats', 'value')
)
def update_content_float_dropdown(feature, float_wmo):  # pylint: disable=W0613
    """Callback which update dropdownFloats value."""
    input_id = get_triggered_id(dash.callback_context)

    if input_id == "float-locations":
        if feature is not None:
            return feature['properties']['filename']

    return float_wmo


# Unsuitable solution to update url path dynamically
# @dash_app.callback(
#     Output("url", "pathname"),
#     [
#         Input("dropdownFloats", "value")
#     ],
#     State('url', 'pathname'),
# )
# def float_location_click(value,pathname):  # pylint: disable=W0613
#     """Callback which update url according to selected float."""
#     input_id = get_triggered_id(dash.callback_context)

#     if input_id == "dropdownFloats":
#         if value is not None:
#             return "/float/" + value

#     return "/"
