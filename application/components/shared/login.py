"""Application login modal component."""
import dash_bootstrap_components as dbc
from dash import html

email_input = html.Div(
    [
        dbc.Label("Login", html_for="username"),
        dbc.Input(type="text", id="username", placeholder="Enter login"),
    ],
    className="mb-3",
)

password_input = html.Div(
    [
        dbc.Label("Password", html_for="password"),
        dbc.Input(
            type="password",
            id="password",
            placeholder="Enter password",
        ),
        dbc.FormFeedback(
            "Connection failed",
            id="password-feedback",
            type="invalid",
        ),
    ],
    className="mb-3",
)

form = dbc.Form(
    [
        email_input,
        password_input,
        dbc.Col(dbc.Button("Submit", color="primary", type="submit"), width="auto"),
    ],
    id="login_form",
)


modal = html.Div(
    [
        dbc.Button("Login", color="primary", className="d-block", id="login", n_clicks=0),
        dbc.Button("Logout", color="danger", className="d-none", id="logout", n_clicks=0),
        dbc.Modal(
            [
                dbc.ModalHeader(dbc.ModalTitle("Login form")),
                dbc.ModalBody(form),
                dbc.ModalFooter(
                    dbc.Button(
                        "Close", id="close-login", className="ms-auto", n_clicks=0
                    )
                ),
            ],
            id="login-modal",
            is_open=False,
        ),
    ]
)
