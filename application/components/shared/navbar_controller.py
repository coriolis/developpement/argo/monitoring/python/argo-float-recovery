"""Navbar controller, contains callbacks from navbar component."""
from dash.dependencies import Input, Output, State
from main import dash_app


# add callback for toggling the collapse on small screens
@dash_app.callback(
    Output("sidebar-collapse", "is_open"),
    [Input("navbar-toggler", "n_clicks")],
    [State("sidebar-collapse", "is_open")],
)
def toggle_sidebar_collapse(num, is_open):
    """Callback that open/close sidebar collapse."""
    if num:
        return not is_open
    return is_open
