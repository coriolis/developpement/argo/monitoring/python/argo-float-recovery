"""Utils general functions."""

from dash._callback_context import CallbackContext


def get_triggered_id(ctx: CallbackContext):
    """Get the triggered element id from dash callback context."""
    if not ctx.triggered:
        return None
    return ctx.triggered[0]['prop_id'].split('.')[0]
