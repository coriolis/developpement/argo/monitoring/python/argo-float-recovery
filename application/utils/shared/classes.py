"""Utils classes."""
from json import JSONEncoder


class JSONAppEncoder(JSONEncoder):
    """Json encoder."""
    def default(self, o):
        """Default methode."""
        return o.__dict__
