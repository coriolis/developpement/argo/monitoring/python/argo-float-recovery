"""Declaration of utils coordinates functions."""
import re


def dms_to_dd(degree, minute, second):
    r"""Convert DMS to DD : dms_str = re.sub(r'\s', '', dms_str)."""
    degree_decimal = degree + float(minute)/60 + float(second)/3600
    return degree_decimal


def ddm_to_dd(ddm_str: str):
    """Convert DDM to DD."""
    dms_str = re.sub(r'\s', '', ddm_str)

    sign = -1 if re.search('[swSW]', dms_str) else 1

    numbers = [*filter(len, re.split(r'\D+', dms_str, maxsplit=4))]

    degree = numbers[0]
    minute_decimal = numbers[1]
    decimal_val = numbers[2] if len(numbers) > 2 else '0'
    minute_decimal += "." + decimal_val

    return sign * (int(degree) + float(minute_decimal) / 60)
