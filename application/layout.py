"""Main app layout."""
from dash import html, dcc

from application.components.shared.navbar import navbar

content = html.Div(
    id="page-content",
    className="container-fluid d-flex flex-column align-items-stretch",
    style={"height": "100vh"}
)

layout = html.Div([
    dcc.Location(id="url"),
    navbar,
    content,
    dcc.Interval(
        id='schedule-component',
        interval=1*1000*60,  # update every minutes
        n_intervals=0
    ),
    dcc.Input(id='authorization', type="hidden"),
])
