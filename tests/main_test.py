"""Auto run tests during conda packaging."""

from main import hello_you


def test_hello_you():
    """Test hello_you func."""
    name = 'Leo'
    assert hello_you(name) == 'Hello Leo'
