# Argo float recovery

Sample application that runs a Dash app mounted inside a FastAPI webserver to recover argo float.

## Cycle de vie du projet

### Branches

Suivre au maximum la [convention de nommages de branche préconisée à Ifremer](https://dev-ops.gitlab-pages.ifremer.fr/documentation/gitlab_quickstart/git/git/#convention-de-nommage-de-branche)

### Processus d'intégration continue

Le processus d'intégration continue mis en place est normalisé car il s'appuie sur le [socle CI/CD Ifremer](https://dev-ops.gitlab-pages.ifremer.fr/templates/automatisation/ci-cd/), \
qui propose un [catalogue de pipelines d'automatisation Java](https://dev-ops.gitlab-pages.ifremer.fr/templates/automatisation/ci-cd/pipelines/java/) \
utilisé dans le pipeline du projet `.gitlab-ci.yml`.

### Version

Pour générer une version de l'application [créer un tag Git](https://dev-ops.gitlab-pages.ifremer.fr/documentation/gitlab_quickstart/gitlab/repository/#creer-une-version). Ceci exécutera automatiquement les tâches automatisées permettant de générer les livrables.

### Release

Une releases Gitlab est créée automatiquement à la création d'une version, elle regroupe les sources de l'application et ses artefacts pour la version en question. La liste des release est consultable depuis la page principale du projet.

### Déploiement

#### Validation

Suivre la [procédure dédiée](https://dev-ops.gitlab-pages.ifremer.fr/hebergement_web/documentation/declaration/deployer-et-tester-une-application-sur-isival/) pour une première intégration de l'application à la plateforme Dockerisée de validation Ifremer.

#### Production

Procédure de mise en production : <https://dev-ops.gitlab-pages.ifremer.fr/hebergement_web/documentation/overview/mex-et-mep/>

## Development

### Docker environment

- Run with docker

```bash
docker compose up
```

### Local environment

1. Install [miniconda](https://doc.ubuntu-fr.org/miniconda)  
2. Create environment if doesn't exists  

   ```bash
   conda create -n argo-float-recovery python=3.11 poetry
   ```

3. Activate environment  

   ```bash
   conda activate argo-float-recovery
   ```

4. Check installation and configure poetry  

   ```bash
   poetry --version
   poetry config repositories.project-registry https://gitlab.ifremer.fr/api/v4/projects/1696/packages/pypi
   ```

### Quickstart

1. Install dependencies

   ```bash
   poetry install
   ```

2. Run webserver

   ```bash
   poetry run python -m main
   ```

3. Load up in browser: http://localhost:8000

### Routes

| Route           | Description                      | Link                            |
|-----------------|----------------------------------|---------------------------------|
| GET /routes     | Base route (tests shared prefix) | http://localhost:8000/routes    |
| GET /status     | App status                       | http://localhost:8000/status    |
| GET /docs       | API documentation                | http://localhost:8000/docs      |
| GET /           | Dash application                 | http://localhost:8000/          |

### Web services

Web services are implemented in a main service file [shared_service.py](https://gitlab.ifremer.fr/coriolis/developpement/argo/monitoring/python/argo-float-recovery/-/blob/develop/application/services/shared_service.py).  

For each new service juste create a new `my_thematic_service.py` and declare it in the main service :  

```python
# Services initialisation
import application.services.my_thematic_service
```

### Components

All compnents are implemented in `components` folder. Each component is a combination of 2 files :

- `component_view.py` : which describe the HTML template  
- `component_controller.py` : which implement the callback linked to this component  

### Configurations

#### ISIVAL configurations

```yaml
  float_recovery:
    name: float-recovery
    url_base_name: floatrecovery
    application_type: custom
    docker_repository: "gitlab-registry.ifremer.fr"
    docker_image_name: argo-float-recovery
    docker_image_path: coriolis/developpement/argo/monitoring/python
    docker_version: develop
    docker_repo_user: gitlab+deploy-token
    docker_repo_password: ${CI_DEPLOY_TOKEN} # replace with deploy token
    application_owner: leo.bruvry.lagadec@ifremer.fr
    environnement_memory: 512m
    environnement_port_mapping:
      host: 9083
      container: 8000
    environnement_volume: |-
      /home/coriolis_exp/spool/co05/co0508/etc/float_recovery:/data:ro, \
      /home/coriolis_exp/binlx/co04/co0414/co041408/dat:/app/config:ro \
    docker_extra_opts: |-
      -e ENVIRONMENT_FILE=.env.production \
      -e HTTP_PROXY="{{vwiz_proxy_host}}:{{vwiz_proxy_port}}" \
      -e HTTPS_PROXY="{{vwiz_proxy_host}}:{{vwiz_proxy_port}}" \
      -e NO_PROXY="localhost,localisation.flotteoceanographique.fr" \
      -e TZ=UTC \
    log_owner: lbruvryl
    log_group: ditiisi
    firewall_config:
      <<: *default_firewall_config
      flux_out:
        - "{{flotte_oceanographique_api}}"
      # Proxy sortant pour :
      # - https://api.vt-explorer.com
      use_proxy: true
```
