const boatColors = {
    'AN': 'rgb(27, 158, 119)',
    'AT': 'rgb(117, 112, 179)',
    'CM': 'rgb(217, 95, 2)',
    'EU': 'rgb(231, 41, 138)',
    'MD': 'rgb(102, 166, 30)',
    'PP': 'rgb(230, 171, 2)',
    'TL': 'rgb(27, 158, 119)',
    'TT': 'rgb(166, 118, 29)',
};

function getRandomRgb() {
    var num = Math.round(0xffffff * Math.random());
    var r = num >> 16;
    var g = num >> 8 & 255;
    var b = num & 255;
    return 'rgb(' + r + ', ' + g + ', ' + b + ')';
}

window.leaflet = Object.assign({}, window.leaflet, {  
    geojsonStyle: {  
        floatFeatureToLayer: function(feature, latlng, context) {  
            const latestOptions = {
                radius: 7,
                fillColor: "#DE0000",
                color: "#333",
                weight: 1,
                opacity: 1,
                fillOpacity: 1,
                boostType: 'circle',
                stroke: true,
                boostScale: 1.5,
                boostExp: 0.125
            };
            
            const basicOptions = {
                radius: 6,
                fillColor: "#f8c844",
                color: "#333",
                weight: 1,
                opacity: 1,
                fillOpacity: 1,
                boostType: 'circle',
                stroke: true,
                boostScale: 1.5,
                boostExp: 0.125
            };
            
            return L.circleMarker(latlng,
                feature.properties['last'] ? latestOptions : basicOptions
            );
        },
        boatLastestFeatureToLayer: function(feature, latlng, context) {  
            var color = undefined;
            if(feature.properties['vessel']['id'] in boatColors){
                color = boatColors[feature.properties['vessel']['id']];
            }else{
                color = getRandomRgb();
                boatColors[feature.properties['vessel']['id']] = color;
            }
            const fontAwesomeIcon = L.divIcon({
                html: '<i class="fa fa-ship fa-2x" style="color:  '+ color +'"></i>',
                iconSize: [20, 20],
                className: 'myDivIcon'
            });
            return L.marker(latlng,{ icon:  fontAwesomeIcon})
        },
        boatFeatureToLayer: function(feature, latlng, context) {            
            var color = "#1C9099";
            if(feature.properties['vessel']['id'] in boatColors){
                color = boatColors[feature.properties['vessel']['id']];
            }
            return L.circleMarker(latlng,
                {
                    radius: 3,
                    fillColor: color,
                    color: "#333",
                    weight: 1,
                    opacity: 1,
                    fillOpacity: 1,
                    boostType: 'circle',
                    stroke: true,
                    boostScale: 1.5,
                    boostExp: 0.125
                }
            );
        }
    }  
});